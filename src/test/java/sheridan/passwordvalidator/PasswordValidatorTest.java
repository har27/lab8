import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PasswordValidatorTest {
	PasswordValidator validator;

	@Before
	public void setUp() throws Exception {
		validator = new PasswordValidator();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGood1IsPasswordValid() {
        String pwd = "Abcdefghij@0";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGood2IsPasswordValid() {
        String pwd = "Bbcdefghij@1";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testGood3IsPasswordValid() {
        String pwd = "Cbcdefghij@2";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testGood4IsPasswordValid() {
        String pwd = "Dbcdefghij@3";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void testBad1IsPasswordValid() {
        String pwd = "abraham";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBad2IsPasswordValid() {
        String pwd = "123456789";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBad3IsPasswordValid() {
        String pwd = "ABRAHAM";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBad4IsPasswordValid() {
        String pwd = "!@@*@*@*";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryIn1IsPasswordValid() {
        String pwd = "Aaaaaa@0";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryIn2IsPasswordValid() {
        String pwd = "Baaaaa@1";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testBoundaryIn3IsPasswordValid() {
        String pwd = "Caaaaa@2";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testBoundaryIn4IsPasswordValid() {
        String pwd = "Daaaaa@3";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut1IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999999";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut2IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999998";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut3IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999997";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut4IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999996";
        boolean actual = validator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}

}