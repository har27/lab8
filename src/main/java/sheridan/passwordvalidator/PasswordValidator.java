public class PasswordValidator {
    static final String SPECIAL_CHARS = "(@,$,+!#?^&)";
    static final String UPPERCASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static final String DIGITS = "01234567890";

    public boolean isPasswordValid(String pwd) {
        if (atLeast8CharsOrDigitsCheck(pwd) && atLeast1SpecialCharCheck(pwd) && atLeast1UppercaseCharCheck(pwd)
                && atLeast1DigitCheck(pwd)) {
            return true;
        }

        return false;
    }

    private boolean atLeast1DigitCheck(String pwd) {
        for (int i = 0; i < pwd.length(); i++) {
            char c = pwd.charAt(i);
            if (DIGITS.indexOf(c) >= 0) {
                return true;
            }
        }
        return false;
    }

    private boolean atLeast1UppercaseCharCheck(String pwd) {
        for (int i = 0; i < pwd.length(); i++) {
            char c = pwd.charAt(i);
            if (UPPERCASE_CHARS.indexOf(c) >= 0) {
                return true;
            }
        }
        return false;
    }

    private boolean atLeast1SpecialCharCheck(String pwd) {
        for (int i = 0; i < pwd.length(); i++) {
            char c = pwd.charAt(i);
            if (SPECIAL_CHARS.indexOf(c) >= 0) {
                return true;
            }
        }
        return false;
    }

    private boolean atLeast8CharsOrDigitsCheck(String pwd) {
        int length = pwd.length();
        return length >= 8;
    }

}