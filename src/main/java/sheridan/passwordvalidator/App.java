/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.passwordvalidator;

/**
 *
 * @author hptop
 */
public class App {
    
    public static void main(String[] args) {
        PasswordValidator pv = new PasswordValidator();
        String goodpwd = "Abcdefghij@0";
        String baddpwd = "abraham";
        String boundaryinpwd = "Aaaaaa@0";
        String boundaryoutpwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^99999999";

        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(goodpwd)));
        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(baddpwd)));
        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(boundaryinpwd)));
        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(boundaryoutpwd)));

    }
}
    

